/**
 * Checkout JavaScript
 * AutomationIL Meetup
 *
 * Copyright (c) 2023. by Way2CU, http://way2cu.com
 * Authors: Mladen Mijatov
 */

window.addEventListener('load', function(event) {
	var checkbox = document.querySelector('div#checkout_container div#input_details div#sign_in.page div.guest_checkout label.checkbox');
	checkbox.style.visibility = 'hidden';
});
